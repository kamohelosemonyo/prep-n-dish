const assert = require('assert')
const validator = require('../../src/util/validation')

describe('Validate unit test', function () {
  it('none object parameter throws exception', function () {
    assert.throws(() => validator.validate(1, {}))
  })

  it('empty object parameter throws exception', function () {
    assert.throws(() => validator.validate({}, {}))
  })

  it('none object metadata parameter throws exception', function () {
    assert.throws(() => validator.validate({ name: 'Simon' }, {}))
  })

  it('empty metadata parameter throws exception', function () {
    assert.throws(() => validator.validate({ name: 'Simon' }, {}))
  })

  it('none matching keys throws exception', function () {
    const object = { name: 'Simon', state: 'pending' }
    const metadata = { name: {}, surname: {} }

    assert.throws(() => validator.validate(object, metadata))
  })

  it('none object metadata keys throws exception', function () {
    const object = { name: 'Simon', state: 'pending' }
    const metadata = { name: 1, surname: {} }

    assert.throws(() => validator.validate(object, metadata))
  })

  it('no metadata key type throws exception', function () {
    const object = { name: 'Simon', state: 'pending' }
    const metadata = { name: {}, surname: {} }

    assert.throws(() => validator.validate(object, metadata))
  })

  it('no errors returns empty array', function () {
    const object = { name: 'Simon', surname: 'says' }
    const metadata = { 
      name: {
        type: 'string'
      },
      surname: {
        type: 'string'
      }
    }

    const expected = []
    const actual = validator.validate(object, metadata)
    assert.deepStrictEqual(expected, actual)
  })

  it('date validation error', function () {
    const object = { today: 'says' }
    const metadata = { today: { type: 'date' } }

    const expected = [{ key: 'today', message: 'invalid date provided for today' }]
    const actual = validator.validate(object, metadata)
    assert.deepStrictEqual(expected, actual)
  })

  it('date validation success', function () {
    const object = { today: '2021-01-10' }
    const metadata = { today: { type: 'date' } }

    const expected = []
    const actual = validator.validate(object, metadata)
    assert.deepStrictEqual(expected, actual)
  })
})

