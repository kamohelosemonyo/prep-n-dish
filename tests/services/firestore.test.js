const assert = require('assert')
const firesore = require('../../src/services/firestore')

describe ('Firestore api wrapper tests', function () {
  const table = 'test'

  it ('list function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.list(table, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('list function called exactly once', async function () {
    const storeStub = getFirebaseStub()
    await firesore.list(table, storeStub)
    assert.deepStrictEqual(storeStub.getCount, 1)
  })

  it ('get function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.get(table, { id: 'numnum'}, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('get function called exactly once', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.getCount, 1)
  })

  it ('get function where key set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.keyName, 'id')
  })

  it ('get function where operator set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.operatorName, '==')
  })

  it ('get function where value set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.get(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.valueName, 'numnum')
  })

  it ('create function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.create(table, { id: 'numnum'}, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('create function object set correctly', async function () {
    const storeStub = getFirebaseStub()
    const obj = { name: 'jim', surname: 'jam'}
    await firesore.create(table, obj, storeStub)
    assert.deepStrictEqual(storeStub.objectRef, obj)
  })

  it ('update function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      const filter =  { id: 'numnum'}
      const obj = { name: 'jim', surname: 'jam'}
      await firesore.update(table, filter, obj, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('update function doc set correctly', async function () {
    const storeStub = getFirebaseStub()
    const filter =  { id: 'numnum'}
    const obj = { name: 'jim', surname: 'jam'}
    await firesore.update(table, filter, obj, storeStub)
    assert.deepStrictEqual(storeStub.docId, filter.id)
  })

  it ('update function object set correctly', async function () {
    const storeStub = getFirebaseStub()
    const filter =  { id: 'numnum'}
    const obj = { name: 'jim', surname: 'jam'}
    await firesore.update(table, filter, obj, storeStub)
    assert.deepStrictEqual(storeStub.objectRef, obj)
  })


  it ('delete function collection is set correctly', async function () {
      const storeStub = getFirebaseStub()
      await firesore.delete(table, { id: 'numnum'}, storeStub)
      assert.deepStrictEqual(storeStub.colname, table)
  })

  it ('delete function called exactly once', async function () {
    const storeStub = getFirebaseStub()
    await firesore.delete(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.deleteCount, 1)
  })

  it ('delete function doc id set correctly', async function () {
    const storeStub = getFirebaseStub()
    await firesore.delete(table, { id: 'numnum'}, storeStub)
    assert.deepStrictEqual(storeStub.docId, 'numnum')
  })
})

function getFirebaseStub () {
  return {
    colname: '',
    docId: '',
    keyName: '',
    operatorName: '',
    valueName: '',
    objectRef: '',
    getCount: 0,
    deleteCount: 0,
    docs: [],
    collection: function (name) {
        this.colname = name
        return this
    },
    doc: function (name) {
        this.docId = name
        return this
    },
    set: function (object) {
      this.objectRef = object
      return this
    },
    update: function (object) {
      this.objectRef = object
      return this
    },
    get: function () { 
      this.getCount = this.getCount + 1
      return this
    },
    delete: function () { 
      this.deleteCount = this.deleteCount + 1
      return this
    },
    where: function (key, operator, value) {
      this.keyName = key
      this.operatorName = operator
      this.valueName = value
      return this
    },
  }
}

const col = getFirebaseStub().collection()