import api from '../services/api'

const recipes = 'recipes'
const ingredients = 'ingredients'
const user = 'user'

export default {
  methods: {
    userLogin: function (username, password) {
      this.$root.toggleLoding()

      api.login(username, password)
        .then(result => {
          this.showSuccess('login successful')
          this.setItem(user, result)
        })
        .catch(err => this.showError(err))
    },
    userLogout: function () {
      api.logout()
       .then(result => this.$root.logout())
       .catch(err => this.showError(err))
    },
    recipeCreate: function (recipe) {
      this.$root.toggleLoding()

      const params = {
        table: recipes,
        object: recipe
      }

      api.create(params)
        .then(result => this.showSuccess(`${recipe.name} created`))
        .catch(err => this.showError(err))
    },
    recipeList: function () {
      this.$root.toggleLoding()

      const params = {
        table: recipes
      }

      api.list(params)
        .then(result => this.setItems(recipes, result))
        .catch(err => this.showError(err))
    },
    recipeUpdate: function (recipe) {
      this.$root.toggleLoding()

      const params = {
        table: recipes,
        filter: { id: recipe.id },
        object: recipe
      }

      api.update()
        .then(result => this.showSuccess(`${recipe.name} updated`))
        .catch(err => this.showError(err))
    },
    recipeUpdateViews: function (recipe) {
      const views = recipe.views + 1

      const params = {
        table: recipes,
        filter: { id: recipe.id },
        object: { views: views }
      }

      api.update(params)
        .then(result => result)
        .catch(err => err)
    },
    recipeDelete: function (recipe) {
      this.$root.toggleLoding()

      const params = {
        table: recipes,
        filter: { id: recipe.id }
      }

      api.delete(params)
        .then(result => this.showSuccess(`${recipe.name} deleted`))
        .catch(err => this.showError(err))
    },
    ingrediantCreate: function (ingredient) {
      this.$root.toggleLoding()

      const params = {
        table: ingredients,
        object: ingredient
      }

      api.create(params)
        .then(result => this.showSuccess('ingredients created'))
        .catch(err => this.showError(err))
    },
    ingrediantGetByRecipe: function (recipeId) {
      const params = {
        table: ingredients,
        filter: { recipe: recipeId }
      }

      api.get(params)
        .then(result => this.setItem(ingredients, result))
        .catch(err => this.showError(err))
    },
    ingrediantUpdate: function (ingredient) {
      this.$root.toggleLoding()

      const params = {
        table: ingredients,
        filter: { id: ingredient.id },
        object: ingredient
      }

      api.update(params)
        .then(result => this.showSuccess('ingredients updated'))
        .catch(err => this.showError(err))
    },
    ingrediantDeleteByRecipe: function (recipeId) {
      this.$root.toggleLoding()

      const params = {
        table: ingredients,
        filter: { recipe: recipeId }
      }

      api.delete(params)
        .then(result => this.showSuccess('ingredients deleted'))
        .catch(err => this.showError(err))
    },
    showSuccess: function (success) {
      this.$root.toggleLoding()
      this.$root.success = success

      setTimeout(() => {
        this.$root.success = ''
      }, 3000)
    },
    showError: function (error) {
      this.$root.toggleLoding()
      if (error instanceof Error) {
        this.$root.error = error.message
      } else {
        this.$root.error = error
      }

      setTimeout(() => {
        this.$root.error = ''
      }, 3000)
    },
    showValidationError: function (error) {
      this.$root.error = error[0]
    },
    setItem: function (keyname, data) {
      this.$root[keyname] = data
    },
    setItems: function (keyname, data) {
      this.$root[keyname] = []
      data.forEach((item) => this.$root[keyname].push(item))
    }
  }
}
