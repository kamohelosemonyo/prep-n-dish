const dbName = 'lelapa'
const storeName = 'state'

const dbObjects = {
  db: null,
  collection: null,
  transaction: null
}

exports.init = function (store = dbObjects) {
  const con = indexedDB.open(dbName, 1)

  con.onupgradeneeded = function () {
    store.db = con.result
    store.collection = store.db.createObjectStore(storeName)
    store.collection.createIndex('id', 'id', { unique: true })
  }

  con.onsuccess = function (event) {
    store.db = con.result
  }
}

exports.save = function (state, store = dbObjects) {
  store.transaction = store.db.transaction(dbName, 'readWrite')
  store.collection = store.transaction.objectStore(storeName)
  store.collection.put(state)
  store.transaction.oncomplete = function () {
    console.log('state saved')
  }
}

exports.get = function (state) {}
