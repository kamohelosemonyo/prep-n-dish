import validation from './validation'

export default {
  methods: {
    validateRecipe: function (recipe) {
      const schema = {
        id: {
          type: 'string'
        },
        name: {
          type: 'alphanumeric'
        },
        views: {
          type: 'numeric'
        },
        datecreated: {
          type: 'date'
        }
      }
      return validation.validate(recipe, schema)
    },
    validateIngredient: function (ingredient) {
      return validation.validate({ value: ingredient }, { value: { type: 'string'} })
    }
  }
}
