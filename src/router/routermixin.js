const paths = require('./paths')

module.exports = {
  methods: {
    gotoHomeView: function () {
      this.$router.push({ name: paths.home.name })
    },
    gotoLoginView: function () {
      this.$router.push({ name: paths.login.name })
    },
    gotoRecipeView: function (recipeName) {
      const urlSafe = toUrlSafeName(recipeName)
      this.$router.push({ name: paths.recipe.name, params: { name: urlSafe } })
    },
    gotoRecipeAdminView: function () {
      this.$router.push({ name: paths.recipeAdmin.name })
    },
    gotoIngredientAdminView: function () {
      this.$router.push({ name: paths.ingredientAdmin.name })
    },
    gotoPrevious: function () {
      this.$router.go(-1)
    },
    routeParamToString (param) {
      return param.replaceAll('-', '')
    }
  }
}

function toUrlSafeName (name) {
  let url = name.replaceAll(' ', '-')
  url = encodeURIComponent(url)
  return url
}
