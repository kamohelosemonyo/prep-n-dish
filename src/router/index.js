import { createRouter, createWebHistory } from 'vue-router'
import { isLoggedIn } from '../services/api'
import paths from './paths'
import Home from '../views/Home.vue'

const routes = [
  {
    path: paths.home.path,
    name: paths.home.name,
    component: Home,
    meta: {
      title: 'Prep n Dish'
    }
  },
  {
    path: paths.login.path,
    name: paths.login.name,
    component: () => import('../views/Login.vue'),
    meta: {
      title: 'Login'
    }
  },
  {
    path: paths.recipe.path,
    name: paths.recipe.name,
    component: () => import('../views/Recipe.vue'),
    meta: {
      title: 'Recipe'
    }
  },
  {
    path: paths.recipeAdmin.path,
    name: paths.recipeAdmin.name,
    component: () => import('../views/admin/RecipeAdmin.vue'),
    meta: {
      title: 'Recipe Admin',
      secure: true
    }
  },
  {
    path: paths.ingredientAdmin.path,
    name: paths.ingredientAdmin.name,
    component: () => import('../views/admin/IngridiantAdmin.vue'),
    meta: {
      title: 'Ingrediant Admin',
      secure: true
    }
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes: routes
})

router.beforeEach(async (to, from, next) => {
  if (to.meta.secure && !await isLoggedIn()) {
    next(paths.login.path)
  } else {
    document.title = to.meta.title
    next()
  }
})

export default router
